package memory

import (
	"time"
)

const (
	// ErrClosed is raised when operating on a cache where Close() has already been called.
	ErrClosed = constError("cache already closed")
	// ErrNotFound indicates that the requested key is not present in the cache
	ErrNotFound = constError("key not found")
)

type constError string

func (err constError) Error() string {
	return string(err)
}

type memCacher interface {
	Get(key string) (interface{}, error)
	SetWithTTL(key string, data interface{}, ttl time.Duration) error
	Close() error
}

type Client struct {
	client memCacher
}

func NewClient(memClient memCacher) *Client {
	m := &Client{}
	m.client = memClient
	return m
}

func (m *Client) Get(key string) []byte {
	val, err := m.client.Get(key)
	if err == ErrNotFound {
		return nil
	}
	return val.([]byte)
}

func (m *Client) Set(key string, data []byte, ttl time.Duration) error {
	return m.client.SetWithTTL(key, data, ttl)
}

func (m *Client) Close() error {
	return m.client.Close()
}
