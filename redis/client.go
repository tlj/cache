package redis

import (
	"context"
	"fmt"
	"time"

	"github.com/go-redis/redis/v8"
)

type client struct {
	client *redis.Client
	prefix string
}

func NewClient(addr, prefix string) *client {
	return &client{
		client: redis.NewClient(&redis.Options{
			Addr:     addr,
			Password: "",
			DB:       0,
		}),
		prefix: prefix,
	}
}

func (r *client) key(key string) string {
	return fmt.Sprintf("p%s#%s", r.prefix, key)
}

func (r *client) Get(key string) []byte {
	res := r.client.Get(context.Background(), r.key(key))
	if res.Err() != nil {
		return nil
	}

	bs, err := res.Bytes()
	if err != nil {
		return nil
	}

	return bs
}

func (r *client) Set(key string, data []byte, ttl time.Duration) error {
	res := r.client.Set(context.Background(), r.key(key), data, ttl)
	return res.Err()
}

func (r *client) Close() error {
	return r.client.Close()
}
