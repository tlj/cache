module gitlab.com/tlj/cache

go 1.17

require (
	github.com/ReneKroon/ttlcache/v2 v2.11.0
	github.com/go-redis/redis/v8 v8.11.4
	github.com/gosimple/slug v1.12.0
)

require (
	github.com/cespare/xxhash/v2 v2.1.2 // indirect
	github.com/dgryski/go-rendezvous v0.0.0-20200823014737-9f7001d12a5f // indirect
	github.com/gosimple/unidecode v1.0.1 // indirect
	golang.org/x/net v0.0.0-20210805182204-aaa1db679c0d // indirect
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c // indirect
	golang.org/x/sys v0.0.0-20210809222454-d867a43fc93e // indirect
	golang.org/x/tools v0.1.7 // indirect
)
