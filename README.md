# Cache Adapters

This repository includes cache adapters for some common ways of caching data, like Redis, filesystem and in-memory.

All adapters are following the same interface which can be easily mocked in your own repository.

## Usage

```golang
package main

import (
	"github.com/ReneKroon/ttlcache/v2"
	"gitlab.com/tlj/cache/filesystem"
	"gitlab.com/tlj/cache/memory"
	"gitlab.com/tlj/cache/redis"
)

func main() {
	var val []byte
	
	fs := filesystem.NewClient("/tmp/cache", "user1")
	fs.Set("mykey", []byte(`my value`), 10)
	val = fs.Get("mykey")
	
	mem := memory.NewClient(ttlcache.NewCache())
	mem.Set("mykey", []byte(`my value`), 10)
	val = mem.Get("mykey")
	
	red := redis.NewClient("localhost", "user1")
	red.Set("mykey", []byte(`my value`), 10)
	val = red.Get("mykey")
}
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)