package filesystem

import (
	"encoding/json"
	"fmt"
	"github.com/gosimple/slug"
	"io/ioutil"
	"syscall"
	"time"
)

type Client struct {
	path   string
	prefix string
}

func NewClient(path, prefix string) *Client {
	return &Client{
		path:   path,
		prefix: prefix,
	}
}

type meta struct {
	CachedAt  time.Time `json:"cached_at"`
	ExpiresAt time.Time `json:"expires_at"`
	TTL       int64     `json:"ttl"`
}

func (c *Client) metaFilename(key string) string {
	key = slug.Make(key)
	if c.prefix != "" {
		return fmt.Sprintf("%s/%s/%s.meta.json", c.path, c.prefix, key)
	} else {
		return fmt.Sprintf("%s/%s.meta.json", c.path, key)
	}
}

func (c *Client) filename(key string) string {
	key = slug.Make(key)
	if c.prefix != "" {
		return fmt.Sprintf("%s/%s/%s.json", c.path, c.prefix, key)
	} else {
		return fmt.Sprintf("%s/%s.json", c.path, key)
	}
}

func (c *Client) remove(key string) error {
	if err := syscall.Unlink(c.metaFilename(key)); err != nil {
		return err
	}

	if err := syscall.Unlink(c.filename(key)); err != nil {
		return err
	}

	return nil
}

func (c *Client) Get(key string) []byte {
	metaData, err := ioutil.ReadFile(c.metaFilename(key))
	if err != nil {
		return nil
	}
	m := &meta{}
	err = json.Unmarshal(metaData, m)
	if err != nil {
		return nil
	}

	if m.TTL > 0 {
		if m.ExpiresAt.Before(time.Now()) {
			_ = c.remove(key)
			return nil
		}
	}

	data, err := ioutil.ReadFile(c.filename(key))
	if err != nil {
		return nil
	}
	return data
}

func (c *Client) Set(key string, data []byte, ttl time.Duration) error {
	m := meta{
		CachedAt:  time.Now(),
		ExpiresAt: time.Now().Add(ttl),
		TTL:       int64(ttl.Seconds()),
	}
	mj, err := json.Marshal(m)
	if err != nil {
		return err
	}
	if err := ioutil.WriteFile(c.metaFilename(key), mj, 0755); err != nil {
		return err
	}
	return ioutil.WriteFile(c.filename(key), data, 0755)
}

func (c *Client) Close() error {
	return nil
}
